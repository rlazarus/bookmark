# modified from http://twistedmatrix.com/documents/current/core/howto/clients.html

from collections import deque
from datetime import date, datetime
from twisted.internet import protocol, reactor
from twisted.words.protocols import irc

def longTime(dateTime):
	if dateTime.date() == date.today():
		return dateTime.strftime("At %H:%M")
	else:
		return dateTime.strftime("On %m/%d at %H:%M")

class BookmarkCollection:
	nextNumber = 1
	bookmarks = deque([])

	def newBookmark(self, nick, date, text):
		bm = (self.nextNumber, nick, date, text)
		self.nextNumber += 1
		return bm

	def push(self, bookmark):
		self.bookmarks.append(bookmark)

	def queue(self, bookmark):
		self.bookmarks.appendleft(bookmark)

	def pop(self):
		return self.bookmarks.pop()

	def isEmpty(self):
		return len(self.bookmarks) == 0

class BookmarkBot(irc.IRCClient):
	nickname = "Bookmark"
	realname = "bot by Shrdlu"
	channels = {"#shrdliarasqui":BookmarkCollection()}
	admins = ["Shrdlu"]

	def signedOn(self):
		self.mode(self.nickname, True, "+B", user=self.nickname)
		for channel in self.channels:
			self.join(channel[1:]) # strip the leading hash

	def joined(self, channel):
		pass

	def privmsg(self, user, channel, msg):
		nick = user.split("!", 1)[0]
		if channel.lower() == self.nickname.lower():
			# Got a PM
			if nick in self.admins:
				if msg.lower() == "quit":
					reactor.stop()
				elif msg.lower().startswith("join"):
					cname = msg[5:]
					if cname[0] != "#":
						cname = "#" + cname
					if cname in self.channels:
						self.msg(nick, "Already in %s." % cname)
					else:
						self.channels[cname] = BookmarkCollection()
						self.join(cname[1:])
						self.msg(nick, "Joined %s." % cname)
				elif msg.lower().startswith("part"):
					cname = msg[5:]
					if cname[0] != "#":
						cname = "#" + cname
					if cname in self.channels:
						del self.channels[cname]
						self.part(cname[1:])
						self.msg(nick, "Parted %s." % cname)
					else:
						self.msg(nick, "Not in %s." % cname)

		else:
			# Got a channel message
			if msg.lower().startswith(self.nickname.lower() + " "):
				msg = msg[(len(self.nickname) + 1):]
			elif msg.lower().startswith(self.nickname.lower() + ", ") or msg.lower().startswith(self.nickname.lower() + ": "):
				msg = msg[(len(self.nickname) + 2):]
			else:
				# not addressed; ignore
				return

			bookmarks = self.channels[channel]
			first = msg.split(" ", 1)[0]
			if first == "pop":
				if bookmarks.isEmpty():
					self.msg(channel, "%s: Nothing queued." % nick)
				else:
					bm = bookmarks.pop()
					self.msg(channel, "%s, %s bookmarked: %s" % (longTime(bm[2]), bm[1], bm[3]))
			elif first == "push":
				if msg == first:
					self.msg(channel, "%s: Push what?" % nick)
				else:
					rest = msg.split(" ", 1)[1]
				bookmarks.push(bookmarks.newBookmark(nick, datetime.now(), rest))
				self.describe(channel, "nods to %s." % nick)
			elif first == "queue":
				if msg == first:
					self.msg(channel, "%s: Queue what?" % nick)
				else:
					rest = msg.split(" ", 1)[1]
				bookmarks.queue(bookmarks.newBookmark(nick, datetime.now(), rest))
				self.describe(channel, "nods to %s." % nick)
			elif first == "list":
				if bookmarks.isEmpty():
					self.msg(channel, "%s: Nothing queued." % nick)
				else:
					for bm in bookmarks.bookmarks:
						self.msg(channel, "%s. %s, %s bookmarked: %s" % (bm[0], longTime(bm[2]), bm[1], bm[3]))
			elif first == "close":
				if msg == first:
					self.msg(channel, "%s: Queue what?" % nick)
				else:
					rest = msg.split(" ", 1)[1]
				try:
					for bm in bookmarks.bookmarks:
						if bm[0] == int(rest):
							bookmarks.bookmarks.remove(bm)
							self.describe(channel, "nods to %s." % nick)
							return
					else:
						self.msg(channel, "%s: Close what?" % nick)
				except ValueError:
					self.msg(channel, "%s: Close what?" % nick)
			else:
				bookmarks.push(bookmarks.newBookmark(nick, datetime.now(), msg))
				self.describe(channel, "nods to %s." % nick)

class BookmarkBotFactory(protocol.ClientFactory):
	def buildProtocol(self, addr):
		protocol = BookmarkBot()
		protocol.factory = self
		return protocol

	def clientConnectionLost(self, connector, reason):
		connector.connect()

	def clientConnectionFailed(self, connector, reason):
		reactor.stop()

if __name__ == '__main__':
	factory = BookmarkBotFactory()
	reactor.connectTCP("irc.foonetic.net", 6667, factory)
	reactor.run()
